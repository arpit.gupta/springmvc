#!/usr/bin/env groovy

def call(BranchName,gitUrl,compile,test,deploy)
{
node
{
    try
    {
        
    stage("CLONE")
    {
        git branch: "${BranchName}" , url: "${gitUrl}"
    }
    if("${compile}"=="true")
        {
    stage("COMPILING")
    {
        sh 'mvn compile'
    }
        }
        if("${test}"=="true")
        {
    stage("TESTING")
    {
        sh 'mvn test'
    }
        }
        
         if("${deploy}"=="true")
        {
    stage("UPLOADING ARTIFACT TO NEXUS")
    {
        sh 'mvn deploy'
    }
        }
    if("${BranchName}"=="dev")
    {
    stage("NOTIFICATON ON SUCCESS ")
    {
        emailext attachLog: true, body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:
Check console output at $BUILD_URL to view the results.
check jenkins at $JENKINS_URL to view jenkins page.
Run on node $NODE_NAME.
Duration of build is $durationString''', subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!', to: 'dev@opstree.com,qa@opstree.com'
    }
    }
    if("${BranchName}"=="QA")
    {
    stage("NOTIFICATON ON SUCCESS ")
    {
        emailext attachLog: true, body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:
Check console output at $BUILD_URL to view the results.
check jenkins at $JENKINS_URL to view jenkins page.
Run on node $NODE_NAME.
Duration of build is $durationString''', subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!', to: 'qa@gmail.com,uat@opstree.com'
    }
    }
    if("${BranchName}"=="UAT")
    {
    stage("NOTIFICATON ON SUCCESS ")
    {
        emailext attachLog: true, body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:
Check console output at $BUILD_URL to view the results.
check jenkins at $JENKINS_URL to view jenkins page.
Run on node $NODE_NAME.
Duration of build is $durationString''', subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!', to:'uat@opstree.com'
    }
    }
    }
    catch(e)
    {
    stage("NOTIFICATON ON FAILURE ")
    {
        emailext attachLog: true, body: '''$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS:
Check console output at $BUILD_URL to view the results.
check jenkins at $JENKINS_URL to view jenkins page.
Run on node $NODE_NAME.
Duration of build is $durationString''', subject: '$PROJECT_NAME - Build # $BUILD_NUMBER - $BUILD_STATUS!', to: 'dev@opstree.com'
    }
    }
}
}
