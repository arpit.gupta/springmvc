#!/usr/bin/env groovy

 def shutdownAndActiveServer(configfolder)
{
    sh "ssh -tt root@192.168.0.103  cp /root/${configfolder}/nginx.conf /etc/nginx/ "
    sh "ssh -tt root@192.168.0.103  systemctl restart nginx"
}


def updateVersion(serverip)
{
    sh "ssh -tt root@${serverip}  wget -r  ${MY_RELEASE_WAR_ARTIFACT_URL} -O /root/tomcat/apache-tomcat-8.5.39/webapps/index.war"
}

def call(strategytype)
{
node
{

if("${strategytype}"=="rolling")
    {
        stage("ShutDown Server1")
            {
            shutdownAndActiveServer("server1stop")
            }

        stage("UPDATE VERSION ${MY_RELEASE_WAR_VERSION} To Server1")
            {
            updateVersion("192.168.0.103")
            }

        stage("Shut Down Server2 And Activate Server1")
            {
             shutdownAndActiveServer("server2stop")
            }


        // stage("UPDATE VERSION ${MY_RELEASE_WAR_VERSION} To Server2")
        //     {
        //     updateVersion("192.168.0.84")
        //     }

        // stage("Activate Server2 with UPDATED VERSION ${MY_RELEASE_WAR_VERSION}")
        //     {
        //     shutdownAndActiveServer("server1server2start")
        //     }
    }
    
    
    

// if (env.deploymentStrategy=="normal")
//     {
//         stage("ShutDown Server1 And Server2 ")
//         {
//         shutdownAndActiveServer("server1server2stop")
//         }

//         stage("UPDATE VERSION ${MY_RELEASE_WAR_VERSION} To Server1")
//         {
//         updateVersion("192.168.0.66")
//         }

//         stage("UPDATE VERSION ${MY_RELEASE_WAR_VERSION} To Server2")
//         {
//         updateVersion("192.168.0.84")   
//         }

//         stage("Activate Server1 And Server2 with UPDATED VERSION ${MY_RELEASE_WAR_VERSION}")
//         {
        
//         shutdownAndActiveServer("server1server2start")
//         }
//     }
    
    

// if (env.deploymentStrategy=="bluegreen")
//     {
//         stage("UPDATE VERSION ${MY_RELEASE_WAR_VERSION} To Server3")
//         {
//          updateVersion("192.168.0.5")
//         }
 
//         stage("UPDATE VERSION ${MY_RELEASE_WAR_VERSION} To Server4")
//         {
//          updateVersion("192.168.0.28")
//         }
 
//         stage("ShutDown Server1,Server2 And Activating Server3,Server4")
//         {
//         shutdownAndActiveServer("server3server4start")
//         }
//     }

// if(env.deploymentStrategy=="batch")
// {
//     stage("ShutDown batch1Servers")
//     {
//         sh '''ssh -tt root@${LoadBalancerIP} \'
//         yes | cp -rf /root/NginxBatch1ServersStop/nginx.conf /etc/nginx/
//         systemctl restart nginx
//         \''''
//     }
    
//     stage("UPDATE VERSION ${MY_RELEASE_WAR_VERSION} To Batch1Servers")
//     {
//         sh '''ssh -tt root@${batch1IP1} "
//             cd /root/tomcat/apache-tomcat-8.5.39/webapps
//             wget ${MY_RELEASE_WAR_ARTIFACT_URL}
//             cd ../bin/
//             ./startup.sh
//             "'''
//             sh '''ssh -tt root@${batch1IP2} "
//             cd /root/tomcat/apache-tomcat-8.5.39/webapps
//             wget ${MY_RELEASE_WAR_ARTIFACT_URL}
//             cd ../bin/
//             ./startup.sh
//             "'''
// }
//  stage("ShutDown batch2Servers And Activate batch1Servers")
// {
// sh '''ssh -tt root@${LoadBalancerIP} \'
//         yes | cp -rf /root/NginxBatch2ServersStop/nginx.conf /etc/nginx/
//         systemctl restart nginx
//         \''''
// }
//  stage("UPDATE VERSION ${MY_RELEASE_WAR_VERSION} To Batch2Servers")
//     {
//         sh '''ssh -tt root@${batch12IP1} "
//             cd /root/tomcat/apache-tomcat-8.5.39/webapps
//             wget ${MY_RELEASE_WAR_ARTIFACT_URL}
//             cd ../bin/
//             ./startup.sh
//             "'''
//             sh '''ssh -tt root@${batch2IP2} "
//             cd /root/tomcat/apache-tomcat-8.5.39/webapps
//             wget ${MY_RELEASE_WAR_ARTIFACT_URL}
//             cd ../bin/
//             ./startup.sh
//             "'''
// }
    
//     stage("Activate Batch1Servers And batch2Servers with UPDATED VERSION ${MY_RELEASE_WAR_VERSION}")
// {
// sh '''ssh -tt root@${LoadBalancerIP} "
// cd /root/NginxBothStart/
// cat nginx_start_content > nginx.conf
// echo return 302 /${MY_RELEASE_WAR_ARTIFACT_ID}-${MY_RELEASE_WAR_VERSION}/ >> nginx.conf
// cat nginx_end_content >> nginx.conf
// yes | cp -rf /root/NginxBothStart/nginx.conf /etc/nginx/
// systemctl restart nginx
// "'''
// }
    
// }
}
}